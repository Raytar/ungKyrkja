package no.ungkyrkja.ungkyrkja.other;

import java.util.Date;

/**
 * Created by JohnIngve on 1/31/2015.
 */
public class UKEntry {

    private Date date;
    private String content;
    private String uk_message;
    private String tek_message;
    private String mat_message;
    private String idrett_message;
    private String band_message;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getUk_message() {
        return uk_message;
    }

    public void setUk_message(String uk_message) {
        this.uk_message = uk_message;
    }

    @Override
    public String toString() {
        return date + ":" + content;
    }

    public String getTek_message() {
        return tek_message;
    }

    public void setTek_message(String tek_message) {
        this.tek_message = tek_message;
    }

    public String getMat_message() {
        return mat_message;
    }

    public void setMat_message(String mat_message) {
        this.mat_message = mat_message;
    }

    public String getIdrett_message() {
        return idrett_message;
    }

    public void setIdrett_message(String idrett_message) {
        this.idrett_message = idrett_message;
    }

    public String getBand_message() {
        return band_message;
    }

    public void setBand_message(String band_message) {
        this.band_message = band_message;
    }
}
