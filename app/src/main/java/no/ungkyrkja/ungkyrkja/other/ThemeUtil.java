package no.ungkyrkja.ungkyrkja.other;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.v7.widget.Toolbar;
import android.view.Window;
import android.view.WindowManager;

import no.ungkyrkja.ungkyrkja.R;

/**
 * Created by johni_000 on 19-03-2015.
 */
public class ThemeUtil{

    // Get the name of the theme from shared preferences

    public String getTheme(Context context){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString("pref_theme", "deep_orange");
    }

    //Set the theme of the application

    public void setTheme(Context context){
        switch(getTheme(context)){
            case "gray":
                context.setTheme(R.style.AppTheme);
                break;
            case "deep_purple":
                context.setTheme(R.style.AppTheme_DeepPurple);
                break;
            case "blue_gray":
                context.setTheme(R.style.AppTheme_BlueGray);
                break;
            case "orange":
                context.setTheme(R.style.AppTheme_Orange);
                break;
            case "deep_orange":
                context.setTheme(R.style.AppTheme_DeepOrange);
                break;
            case "blue":
                context.setTheme(R.style.AppTheme_Blue);
                break;
            case "green":
                context.setTheme(R.style.AppTheme_Green);
                break;
            default:
                break;
        }
    }

    //change the color of the toolbar

    public Toolbar setActionBarTheme(Context context, Toolbar toolbar){
        switch(getTheme(context)) {
            case "gray":
                toolbar.setBackgroundResource(R.color.primary);
                return toolbar;
            case "deep_purple":
                toolbar.setBackgroundResource(R.color.primary_deep_purple);
                return toolbar;
            case "blue_gray":
                toolbar.setBackgroundResource(R.color.primary_bluegray);
                return toolbar;
            case "orange":
                toolbar.setBackgroundResource(R.color.primary_orange);
                return toolbar;
            case "deep_orange":
                toolbar.setBackgroundResource(R.color.primary_deep_orange);
                return toolbar;
            case "blue":
                toolbar.setBackgroundResource(R.color.primary_blue);
                return toolbar;
            case "green":
                toolbar.setBackgroundResource(R.color.primary_green);
                return toolbar;
            default:
                return toolbar;
        }
    }



    public int getStatusBarColor(Context context){
        switch(getTheme(context)){
            case "gray":
                return R.color.primary_dark;
            case "deep_purple":
                return R.color.primary_dark_deep_purple;
            case "blue_gray":
                return R.color.primary_dark_bluegray;
            case "orange":
                return R.color.primary_dark_orange;
            case "deep_orange":
                return R.color.primary_dark_deep_orange;
            case "blue":
                return R.color.primary_dark_blue;
            case "green":
                return R.color.primary_dark_green;
            default:
                return R.color.primary_dark;
        }
    }

    //change the color of the statusbar

    public void setStatusBarColor(Activity activity){
        Context context = activity.getApplicationContext();
        Window window = activity.getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(context.getResources().getColor(getStatusBarColor(context)));
        }
    }
}
