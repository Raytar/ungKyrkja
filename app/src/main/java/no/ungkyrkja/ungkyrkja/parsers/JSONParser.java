package no.ungkyrkja.ungkyrkja.parsers;

import android.content.Context;
import android.util.JsonReader;
import android.util.JsonToken;
import no.ungkyrkja.ungkyrkja.R;
import no.ungkyrkja.ungkyrkja.other.UKEntry;

import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by JohnIngve on 22.03.2015.
 */
public class JSONParser {

    public Context context;
    public static String KEY_DATE="date";
    public static String KEY_CONTENT="content";
    public static String KEY_UK_MESSAGE="uk_message";
    public static String KEY_TEKNIKER_MESSAGE="tek_message";
    public static String KEY_MAT_MESSAGE="mat_message";
    public static String KEY_IDRETT_MESSAGE="idrett_message";
    public static String KEY_BAND_MESSAGE="band_message";
    public DateFormat format;

    public JSONParser(Context context){
        this.context = context;
        this.format = new SimpleDateFormat(context.getResources().getString(R.string.format_date), Locale.ENGLISH);
    }

    public List<UKEntry> getEntriesFromFile(String fileName) throws IOException, ParseException, NullPointerException{
        List<UKEntry> entries = new ArrayList<>();
        JsonReader reader = new JsonReader(new InputStreamReader(context.openFileInput(fileName), "UTF-8"));
        try{
            reader.beginArray();
            while(reader.hasNext()){
                UKEntry currentEntry = readEntry(reader);
                if(currentEntry != null){
                    entries.add(currentEntry);
                }
            }
        }
        finally {
            reader.close();
        }
        return entries;
    }

    private UKEntry readEntry(JsonReader reader) throws IOException, ParseException, NullPointerException{
        UKEntry currentEntry = new UKEntry();
        Date today;
        Date date;
        reader.beginObject();
        while (reader.hasNext()){
            String name = reader.nextName();
            if(name.equals(KEY_DATE)){
                //If date is null, ignore the event
                if(reader.peek() == JsonToken.NULL){
                    reader.endObject();
                    return null;
                }
                //Get Current date
                today = new Date(Calendar.YEAR, Calendar.MONTH, Calendar.DAY_OF_MONTH);
                //Get event date
                date = format.parse(reader.nextString());
                //Remove completed events
                if (date.compareTo(today)>=0){
                    currentEntry.setDate(date);
                }
                else{
                    reader.endObject();
                    return null;
                }
            }
            else if (name.equals(KEY_CONTENT) && reader.peek() != JsonToken.NULL){
                currentEntry.setContent(nextString(reader));
            }
            else if (name.equals(KEY_UK_MESSAGE) && reader.peek() != JsonToken.NULL){
                currentEntry.setUk_message(nextString(reader));
            }
            else if (name.equals(KEY_TEKNIKER_MESSAGE) && reader.peek() != JsonToken.NULL){
                currentEntry.setTek_message(nextString(reader));
            }
            else if (name.equals(KEY_MAT_MESSAGE) && reader.peek() != JsonToken.NULL){
                currentEntry.setMat_message(nextString(reader));
            }
            else if (name.equals(KEY_IDRETT_MESSAGE) && reader.peek() != JsonToken.NULL){
                currentEntry.setIdrett_message(nextString(reader));
            }
            else if (name.equals(KEY_BAND_MESSAGE) && reader.peek() != JsonToken.NULL){
                currentEntry.setBand_message(nextString(reader));
            }
            else reader.skipValue();
        }
        reader.endObject();
        return currentEntry;
    }

    private String nextString(JsonReader reader) throws IOException{
        String string = reader.nextString();
        if (!string.equals("")){
            return string;
        }
        return null;
    }
}
