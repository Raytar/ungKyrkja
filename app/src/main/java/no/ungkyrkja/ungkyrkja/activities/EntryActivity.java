package no.ungkyrkja.ungkyrkja.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import no.ungkyrkja.ungkyrkja.R;
import no.ungkyrkja.ungkyrkja.adapters.EntryListAdapter;
import no.ungkyrkja.ungkyrkja.other.EntryItem;
import no.ungkyrkja.ungkyrkja.other.ThemeUtil;


public class EntryActivity extends AppCompatActivity{

    Toolbar toolbar;
    RecyclerView mRecyclerView;
    EntryListAdapter adapter;

    private static Date date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ThemeUtil themeUtil = new ThemeUtil();
        themeUtil.setTheme(this);
        themeUtil.setStatusBarColor(this);
        setContentView(R.layout.activity_entry);

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        toolbar = themeUtil.setActionBarTheme(this, toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);

        mRecyclerView = (RecyclerView) findViewById(R.id.entry_list);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String pref_group = preferences.getString("pref_group", "group_none");
        String[] groupNames = getResources().getStringArray(R.array.group_names);

        Intent intent= getIntent();
        Bundle extras = intent.getExtras();
        SimpleDateFormat format = new SimpleDateFormat(getResources().getString(R.string.format_date), Locale.ENGLISH);
        String[] months = getResources().getStringArray(R.array.months);

        adapter = new EntryListAdapter(this, loadEntryList(extras, format, months, pref_group, groupNames));
        mRecyclerView.setAdapter(adapter);
    }

    private List<EntryItem> loadEntryList(Bundle items, SimpleDateFormat dateFormat, String[] months, String pref_group, String[] groupNames) {
        List<EntryItem> entryItems = new ArrayList<>();

        try {
            date = dateFormat.parse(items.getString("EXTRA_DATE_STRING"));
        } catch (ParseException e) {
            e.printStackTrace();
            Log.e("EntryActivity", "Failed to parse date-string");
        }
        String content = items.getString("EXTRA_CONTENT");
        String message = items.getString("EXTRA_MESSAGE");
        String tek_message = items.getString("EXTRA_GROUP_TEK");
        String idrett_message = items.getString("EXTRA_GROUP_IDRETT");
        String mat_message = items.getString("EXTRA_GROUP_MAT");
        String band_message = items.getString("EXTRA_GROUP_BAND");
        String minutes;

        entryItems.add(new EntryItem(R.drawable.ic_today_grey600_24dp, date.getDate()+". "+months[date.getMonth()]));
        if(date.getMinutes()<10){
            minutes = "0"+date.getMinutes();
        }
        else{
            minutes = ""+date.getMinutes();
        }
        entryItems.add(new EntryItem(R.drawable.ic_access_time_grey600_24dp, date.getHours() + ":" + minutes));
        if(content!=null){
            entryItems.add(new EntryItem(R.drawable.ic_label_grey600_24dp, content));
        }
        if(message != null) {
            entryItems.add(new EntryItem(R.drawable.ic_comment_grey600_24dp, message));
        }

        switch (pref_group){
            case "group_none":
                if(tek_message != null){
                    entryItems.add(new EntryItem(R.drawable.ic_group_grey600_24dp, groupNames[1]+": "+tek_message));
                }
                if(idrett_message != null){
                    entryItems.add(new EntryItem(R.drawable.ic_group_grey600_24dp, groupNames[2]+": "+idrett_message));
                }
                if(mat_message != null){
                    entryItems.add(new EntryItem(R.drawable.ic_group_grey600_24dp, groupNames[3]+": "+mat_message));
                }
                if(band_message != null){
                    entryItems.add(new EntryItem(R.drawable.ic_group_grey600_24dp, groupNames[4]+": "+band_message));
                }
                break;
            case "group_tekniker":
                if(tek_message != null){
                    entryItems.add(new EntryItem(R.drawable.ic_group_grey600_24dp, groupNames[1]+": "+tek_message));
                }
                break;
            case "group_idrett":
                if(idrett_message != null){
                    entryItems.add(new EntryItem(R.drawable.ic_group_grey600_24dp, groupNames[2]+": "+idrett_message));
                }
                break;
            case "group_mat":
                if(mat_message != null){
                    entryItems.add(new EntryItem(R.drawable.ic_group_grey600_24dp, groupNames[3]+": "+mat_message));
                }
                break;
            case "group_band":
                if(band_message != null){
                    entryItems.add(new EntryItem(R.drawable.ic_group_grey600_24dp, groupNames[4]+": "+band_message));
                }
                break;
            default:
                entryItems.add(new EntryItem(R.drawable.ic_group_grey600_24dp, ""+R.string.notify_select_group));
        }
        return entryItems;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

       if (id==android.R.id.home){
           NavUtils.navigateUpFromSameTask(this);
        }
        return super.onOptionsItemSelected(item);
    }

}
