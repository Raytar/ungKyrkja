package no.ungkyrkja.ungkyrkja.network;

import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by JohnIngve on 23.02.2015.
 */
public class DownloadTask {

    public Boolean download(String url, String fileName, Context context) throws IOException{
        Boolean successfull = false;
        URL downloadURL;
        HttpURLConnection connection = null;
        Reader inputStream = null;
        OutputStreamWriter out = null;
        try{

            downloadURL = new URL(url);
            connection = (HttpURLConnection) downloadURL.openConnection();
            inputStream = new InputStreamReader(connection.getInputStream(), "UTF-8");


            out = new OutputStreamWriter(context.openFileOutput(fileName, Context.MODE_PRIVATE), "UTF-8");
            int read = -1;
            char[] buffer = new char[1024];
            while ((read = inputStream.read(buffer)) != -1){
                out.write(buffer, 0, read);
            }
            successfull=true;
        }
        finally{
            if(connection != null){
                connection.disconnect();
            }
            if(inputStream!=null){
                inputStream.close();
            }
            if (out != null){
                out.close();
            }
        }
        Log.d("DownloadTask", "Download Successfull");
        return successfull;
    }
}
