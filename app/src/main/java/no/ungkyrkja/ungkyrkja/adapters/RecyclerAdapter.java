package no.ungkyrkja.ungkyrkja.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Date;
import java.util.List;

import no.ungkyrkja.ungkyrkja.R;
import no.ungkyrkja.ungkyrkja.other.UKEntry;

/**
 * Created by JohnIngve on 24.02.2015.
 */
public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.EntryListRowHolder>{

    private List<UKEntry> entries;
    private LayoutInflater inflater;
    private ClickListener clickListener;
    private String[] months;

    public RecyclerAdapter(Context context, List<UKEntry> entries){
        this.entries = entries;
        inflater = LayoutInflater.from(context);
        this.months = context.getResources().getStringArray(R.array.months);

    }

    @Override
    public EntryListRowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.entry_card_layout, parent, false);
        return new EntryListRowHolder(view);
    }

    @Override
    public void onBindViewHolder(EntryListRowHolder EntryListRowHolder, int i) {
        UKEntry entry = entries.get(i);

        Date date = entry.getDate();

        EntryListRowHolder.date.setText(date.getDate()+". "+months[date.getMonth()]);

        EntryListRowHolder.content.setText(entry.getContent());

    }

    public void setClickListener(ClickListener clickListener){
        this.clickListener=clickListener;
    }

    @Override
    public int getItemCount() {
        return (null != entries ? entries.size() : 0);
    }

    public interface ClickListener{
        public void itemClicked(View view, int position);
    }

    public class EntryListRowHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        protected TextView date;
        protected TextView content;

        public EntryListRowHolder(View view){
            super(view);
            view.setOnClickListener(this);
            this.date = (TextView) view.findViewById(R.id.card_text_date);
            this.content = (TextView) view.findViewById(R.id.card_text_content);
        }

        @Override
        public void onClick(View v) {
            if(clickListener!=null) {
                clickListener.itemClicked(v, getPosition());
            }
        }
    }
}
