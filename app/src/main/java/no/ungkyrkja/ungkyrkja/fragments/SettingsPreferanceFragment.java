package no.ungkyrkja.ungkyrkja.fragments;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.support.v4.app.NavUtils;

import no.ungkyrkja.ungkyrkja.R;

/**
 * Created by JohnIngve on 23.02.2015.
 */
public class SettingsPreferanceFragment extends PreferenceFragment implements OnSharedPreferenceChangeListener{

    Activity activity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = getActivity();
        addPreferencesFromResource(R.xml.settings);
        ListPreference listPrefGroup = (ListPreference) getPreferenceScreen().findPreference("pref_group");
        listPreferenceSummaryChanger(listPrefGroup, getResources().getString(R.string.pref_set_group_summary));
        ListPreference listPrefTheme = (ListPreference) getPreferenceScreen().findPreference("pref_theme");
        listPreferenceSummaryChanger(listPrefTheme, getResources().getString(R.string.pref_theme_summary));

        SharedPreferences sharedPreferences = getPreferenceScreen().getSharedPreferences();
        sharedPreferences.registerOnSharedPreferenceChangeListener(this);
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Preference pref = findPreference(key);

        if (pref instanceof ListPreference) {
            ListPreference listPref = (ListPreference) pref;
            pref.setSummary(listPref.getEntry());
        }

        if (pref.getKey().equals("pref_theme")){
            NavUtils.navigateUpFromSameTask(activity);
        }
    }

    public void listPreferenceSummaryChanger(ListPreference listPref, String defaultSummary){
        if (listPref.getEntry()!=null){
            listPref.setSummary(listPref.getEntry());
        }
        else {listPref.setSummary(defaultSummary);}
    }
}