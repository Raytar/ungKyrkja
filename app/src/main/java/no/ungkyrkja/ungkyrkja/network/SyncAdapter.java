package no.ungkyrkja.ungkyrkja.network;

import android.accounts.Account;
import android.content.*;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import no.ungkyrkja.ungkyrkja.R;
import no.ungkyrkja.ungkyrkja.other.UKEntry;

import java.util.List;

/**
 * Created by JohnIngve on 21.05.2015.
 */
public class SyncAdapter extends AbstractThreadedSyncAdapter{
    private final ContentResolver mContentResolver;

    public SyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        mContentResolver = context.getContentResolver();
    }

    public SyncAdapter(Context context, boolean autoInitialize, boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
        mContentResolver = context.getContentResolver();
    }

    @Override
    public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) {
        String[] params = new String[2];
        params[0] = getContext().getResources().getString(R.string.calendar_url);
        params[1] = "uk.json";
        DownloadThread downloadTask = new DownloadThread();
        downloadTask.execute(params);
    }
    public class DownloadThread extends AsyncTask<String, Void, Boolean> {

        Context context;
        List<UKEntry> entries;

        @Override
        protected Boolean doInBackground(String... params) {
            Boolean successful;
            String url = params[0];
            String fileName = params[1];
            DownloadTask downloadTask = new DownloadTask();
            try {
                successful = downloadTask.download(url, fileName, context);
            } catch (Exception e) {
                e.printStackTrace();
                successful=false;
            }
            return successful;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            context = getContext();
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(!aBoolean){
                Log.e("UKSync", "Failed to update calendar");
            }
        }
    }
}
