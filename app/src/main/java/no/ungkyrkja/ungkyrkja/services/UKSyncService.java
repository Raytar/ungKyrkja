package no.ungkyrkja.ungkyrkja.services;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import no.ungkyrkja.ungkyrkja.network.Authenticator;

/**
 * Created by JohnIngve on 21.05.2015.
 */
public class UKSyncService extends Service{

    private final IBinder mBinder = new SyncBinder();


    public class SyncBinder extends Binder{
        public UKSyncService getService(){
            return UKSyncService.this;
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("UKSyncService", "Recieved start id"+startId+": "+intent);
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }
}
