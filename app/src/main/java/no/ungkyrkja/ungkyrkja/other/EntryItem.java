package no.ungkyrkja.ungkyrkja.other;

public class EntryItem{
    private int drawableID;
    private String title;

    public EntryItem(){

    }

    public EntryItem(int drawableID, String title){
        this.drawableID = drawableID;
        this.title = title;
    }

    public int getDrawableID() {
        return drawableID;
    }

    public void setDrawableID(int drawableID) {
        this.drawableID = drawableID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}