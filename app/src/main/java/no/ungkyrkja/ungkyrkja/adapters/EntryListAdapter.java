package no.ungkyrkja.ungkyrkja.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import no.ungkyrkja.ungkyrkja.R;
import no.ungkyrkja.ungkyrkja.other.EntryItem;

/**
 * Created by JohnIngve on 3/31/2015.
 */
public class EntryListAdapter extends RecyclerView.Adapter<EntryListAdapter.ItemViewHolder>{

    private List<EntryItem> items = Collections.emptyList();
    private LayoutInflater inflater;
    private ClickListener clickListener;

    public EntryListAdapter(Context context, List<EntryItem> items) {
        this.items = items;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.entry_item, parent, false);
        ItemViewHolder holder = new ItemViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        EntryItem item = items.get(position);
        holder.title.setText(item.getTitle());
        holder.icon.setImageResource(item.getDrawableID());
    }

    public void setClickListener(ClickListener clickListener){
        this.clickListener=clickListener;
    }

    @Override
    public int getItemCount() {
        return (null != items ? items.size() : 0);
    }

    public interface ClickListener{
        public void itemClicked(View itemView, int position);
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        protected TextView title;
        protected ImageView icon;

        public ItemViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            this.title = (TextView) itemView.findViewById(R.id.entry_item_text);
            this.icon = (ImageView) itemView.findViewById(R.id.entry_item_img);
        }

        @Override
        public void onClick(View v) {
            if(clickListener!=null){
                clickListener.itemClicked(v, getPosition());
            }
        }
    }
}
