# ungKyrkja
The ungKyrkja application

<h3>Authors</h3>
<a href="http://github.com/raytar">- Raytar</a><br>
<a href="http://github.com/danieltafjord">- Daniel Tafjord</a>

<h4>To do list</h4>
- [] make companion website work
- [] Add background service
- [] Add Sync adapter
- [] Add stub Authenticator
- [] Add stub Content provider
- [] Add songtexts
- [] Add kiosk calculator (for fun)
