package no.ungkyrkja.ungkyrkja.activities;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import no.ungkyrkja.ungkyrkja.R;
import no.ungkyrkja.ungkyrkja.adapters.RecyclerAdapter;
import no.ungkyrkja.ungkyrkja.network.ConnectionDetector;
import no.ungkyrkja.ungkyrkja.network.DownloadTask;
import no.ungkyrkja.ungkyrkja.other.DividerItemDecoration;
import no.ungkyrkja.ungkyrkja.other.ThemeUtil;
import no.ungkyrkja.ungkyrkja.other.UKEntry;
import no.ungkyrkja.ungkyrkja.parsers.JSONParser;


public class MainActivity extends AppCompatActivity {
    public List<UKEntry> entries = null;
    private String url;
    private static final String fileName = "uk.json";
    private RecyclerView mRecyclerView;
    private MenuItem refreshItem;
    private boolean isCurrentlyLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ThemeUtil themeUtil = new ThemeUtil();
        themeUtil.setTheme(this);
        themeUtil.setStatusBarColor(this);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        toolbar = themeUtil.setActionBarTheme(this, toolbar);
        toolbar.setTitle(getResources().getString(R.string.title_activity_main));
        setSupportActionBar(toolbar);

        url = getResources().getString(R.string.calendar_url);

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getResources().getDrawable(R.drawable.line_divider)));

        if (isConnected(this, PreferenceManager.getDefaultSharedPreferences(this))) {
            refreshContent();
        } else {
            Toast.makeText(this, "No internet connection", Toast.LENGTH_SHORT).show();
            refreshRecyclerView(mRecyclerView, this);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_about) {
            startActivity(new Intent(this, AboutActivity.class));
        } else if (id == R.id.action_settings) {
            startActivity(new Intent(MainActivity.this, SettingsActivity.class));
        } else if (id == R.id.action_refresh) {
            //Start animation
            startRefreshing(item);
            //Start refreshing
            if (isConnected(this, PreferenceManager.getDefaultSharedPreferences(this))) {
                refreshContent();
            } else {
                Toast.makeText(this, "No internet connection", Toast.LENGTH_SHORT).show();
                refreshRecyclerView(mRecyclerView, this);
            }
        }

        return super.onOptionsItemSelected(item);
    }

    private boolean isConnected(Context context, SharedPreferences sharedPreferences) {
        return sharedPreferences.getBoolean("pref_mobiledata", false) && ConnectionDetector.isConnectedMobile(context) || ConnectionDetector.isConnectedWifi(context);
    }

    private void refreshContent() {
        String[] params = new String[2];
        params[0] = url;
        params[1] = fileName;
        MainDownloadTask downloadTask = new MainDownloadTask();
        downloadTask.execute(params);
    }

    private void refreshRecyclerView(RecyclerView recyclerView, final Context context) {
        JSONParser jsonParser = new JSONParser(context);
        try {
            entries = jsonParser.getEntriesFromFile(fileName);
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, "Download failed", Toast.LENGTH_SHORT).show();
        }
        RecyclerAdapter adapter = new RecyclerAdapter(context, entries);
        adapter.setClickListener(new RecyclerAdapter.ClickListener() {
            @Override
            public void itemClicked(View view, int position) {
                Intent intent = new Intent(context, EntryActivity.class);
                Bundle extras = new Bundle();
                UKEntry clickedEntry = entries.get(position);
                Date date = clickedEntry.getDate();
                SimpleDateFormat format = new SimpleDateFormat(getResources().getString(R.string.format_date), Locale.ENGLISH);
                extras.putString("EXTRA_DATE_STRING", format.format(date));
                if(clickedEntry.getContent()!=null){
                    extras.putString("EXTRA_CONTENT", clickedEntry.getContent());
                }
                if(clickedEntry.getUk_message()!=null){
                    extras.putString("EXTRA_MESSAGE", clickedEntry.getUk_message());
                }
                if(clickedEntry.getTek_message()!=null){
                    extras.putString("EXTRA_GROUP_TEK", clickedEntry.getTek_message());
                }
                if(clickedEntry.getIdrett_message()!=null){
                    extras.putString("EXTRA_GROUP_IDRETT", clickedEntry.getIdrett_message());
                }
                if(clickedEntry.getMat_message()!=null){
                    extras.putString("EXTRA_GROUP_MAT", clickedEntry.getMat_message());
                }
                if(clickedEntry.getBand_message()!=null){
                    extras.putString("EXTRA_GROUP_BAND", clickedEntry.getBand_message());
                }
                intent.putExtras(extras);
                startActivity(intent);
            }
        });
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        isCurrentlyLoading = false;
    }

    public void startRefreshing(MenuItem item){
        isCurrentlyLoading = true;
        refreshItem = item;
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        ImageView iv = (ImageView) inflater.inflate(R.layout.action_refresh, null);
        Animation rotation = AnimationUtils.loadAnimation(this, R.anim.actionbar_rotate);
        rotation.setRepeatCount(Animation.INFINITE);
        rotation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                if (!isCurrentlyLoading) {
                    refreshItem.getActionView().clearAnimation();
                    refreshItem.setActionView(null);
                }
            }
        });
        iv.startAnimation(rotation);
        item.setActionView(iv);
    }

    public class MainDownloadTask extends AsyncTask<String, Void, Boolean> {

        Context context;

        @Override
        protected Boolean doInBackground(String... params) {
            Boolean successful;
            String url = params[0];
            String fileName = params[1];
            DownloadTask downloadTask = new DownloadTask();
            try {
                successful = downloadTask.download(url, fileName, context);
            } catch (Exception e) {
                e.printStackTrace();
                successful=false;
            }

            return successful;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            context = getApplicationContext();
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(!aBoolean){
                Toast.makeText(context, "Download failed", Toast.LENGTH_SHORT).show();
            }
            refreshRecyclerView(mRecyclerView, MainActivity.this);
        }
    }

}

