package no.ungkyrkja.ungkyrkja.activities;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import no.ungkyrkja.ungkyrkja.R;
import no.ungkyrkja.ungkyrkja.fragments.SettingsPreferanceFragment;
import no.ungkyrkja.ungkyrkja.other.ThemeUtil;


public class SettingsActivity extends AppCompatActivity {

    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ThemeUtil themeUtil = new ThemeUtil();
        themeUtil.setTheme(this);
        themeUtil.setStatusBarColor(this);
        setContentView(R.layout.activity_settings);
        //set the toolbar and enable the home button
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        toolbar = themeUtil.setActionBarTheme(this, toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Display the fragment below the app bar
        FragmentManager mFragmentManager = getFragmentManager();
        FragmentTransaction mFragmentTransaction = mFragmentManager.beginTransaction();
        SettingsPreferanceFragment fragment = new SettingsPreferanceFragment();
        mFragmentTransaction.replace(R.id.settings_linearlayout, fragment);
        mFragmentTransaction.commit();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id==android.R.id.home){
            NavUtils.navigateUpFromSameTask(this);
        }

        return super.onOptionsItemSelected(item);
    }
}
